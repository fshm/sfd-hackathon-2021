(ns hackathon.db
    (:require [clojure.edn :as edn]
              [hikari-cp.core :as hikari]
;;            [clojure.java.jdbc :as jdbc]
            [ragtime.jdbc :as ragtime]))

(def datasource-options (edn/read-string (slurp "resources/env/jdbc.edn")))

(defonce datasource
  (delay (hikari/make-datasource datasource-options)))

(def jdbc {:datasource @datasource})

(def config
  {:datastore  (ragtime/sql-database jdbc)
   :migrations (ragtime/load-resources "migrations")})
