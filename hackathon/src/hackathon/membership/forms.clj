(ns hackathon.membership.forms)

(defn membership-form []
  [:div#sub-duo
   [:div#member-1
    [:div.text-center
     [:span {:style "font-size: 1.5rem;"}]
     [:span {:style "margin-left: 12px; font-size: 1.5rem;"} [:strong "Registration"]]]
    [:div#member-3
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-user]]
      [:input.form-control {:name "name" :required "required"  :type "text" :placeholder "Name"}]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]] [:input.form-control {:name "email" :required "required" :type "email" :placeholder "E-Mail Address"}]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-phone]] [:input.form-control {:name "phone" :required "required" :type "tel" :placeholder "Phone Number"}]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-graduation-cap]]
      [:input.form-control {:name "institute" :required "required" :type "text" :placeholder "Institution/College"}]]]]])
