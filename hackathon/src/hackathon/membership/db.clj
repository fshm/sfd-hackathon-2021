(ns hackathon.membership.db
    (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hackathon/membership/db.sql")
