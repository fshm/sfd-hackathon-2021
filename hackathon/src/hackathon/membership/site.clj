(ns hackathon.membership.site
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [hackathon.site :as site]
            [hackathon.db :as db]
            [hackathon.membership.db :as memberships.db]
            [hackathon.membership.forms :as memberships.forms]))

(defn membership-page [request]
  (ring-resp/response
   (site/base
    "Membership Form"
    [:div
     [:section.page-section.cta
      [:div.container
       [:div.row
        [:div.col-xl-9.mx-auto
         [:div.cta-inner.mx-auto.bg-faded.rounded.wrapper.w-100
          [:form {:action "/membership" :method "post"}
           (site/form-anti-forgery-token request)
           [:h2.section-heading.mb-5.text-center [:span.section-heading-lower "Membership"]]
           (memberships.forms/membership-form)
           [:input.btn.btn-primary.mb-3 {:type "submit" :value "Submit"}]
           ]]]]]]])))

(defn membership-post [request]
  (let [form-params (:form-params request)]
    (memberships.db/memberships-insert
     db/jdbc
     form-params)
    (ring-resp/redirect "/")))
