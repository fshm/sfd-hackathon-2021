-- :name memberships-insert :! :n
-- :doc Insert a single membership
insert into memberships
values (:name, :phone, :email, :institute)

-- :name memberships-all :? :*
-- :doc Get all memberships
select * from memberships

-- :name memberships-by- :? :1
-- :doc Get memberships by email
select * from memberships
where email = :email
