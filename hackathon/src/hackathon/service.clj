(ns hackathon.service
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [io.pedestal.http.csrf :as csrf]
            [io.pedestal.interceptor :as interceptor]
            [io.pedestal.interceptor.chain :as interceptor.chain]
            [io.pedestal.interceptor.error :refer [error-dispatch]]
            [ring.util.response :as ring-resp]
            [hackathon.interceptors :as interceptors]
            [hackathon.site.home :as home]
            [hackathon.membership.site :as membership]
            [hackathon.auth.users.site :as users.site]
            [hackathon.auth.routes :as auth.routes]
            [ring.middleware.session.cookie :as cookie]
            [buddy.auth :as auth]
            [buddy.auth.backends :as auth.backends]
            [buddy.auth.middleware :as auth.middleware]
            [cheshire.core :as json]
            ;; [mount.core :refer defstate]
            ))


(def secret (:secret (clojure.edn/read-string (slurp "resources/env/secret.edn"))))

(defn unauthorized-handler
  "This is the default action for buddy-auth's unathorized exception."
  [request metadata]
  (ring-resp/redirect "/"))

(def backend (auth.backends/session {:unauthorized-handler unauthorized-handler}))


(defn authorized  "Returns a 200 response for authorized users, otherwise throws a buddy-auth 'unauthorized' exception."
  [request]
    (if (auth/authenticated? request)
      (ring-resp/response  "Only users can see this!")
      (auth/throw-unauthorized)))


(defn about-page
  [request]
  (ring-resp/response (format "Clojure %s - served from %s"
                              (clojure-version)
                              (route/url-for ::about-page))))

;; Tabular routes
(def routes
  (clojure.set/union
   #{["/" :get (conj interceptors/common-interceptors `home/home-page)]
     ["/submission" :get (conj interceptors/common-interceptors `home/submission-page)]
     ["/submission" :post (conj interceptors/common-interceptors `home/submission-post)]
     ["/registration" :get (conj interceptors/common-interceptors `home/registration-page)]
     ["/registration" :post (conj interceptors/common-interceptors `home/registration-post)]
     ["/membership" :get (conj interceptors/common-interceptors `membership/membership-page)]
     ["/membership" :post (conj interceptors/common-interceptors `membership/membership-post)]
     ["/instructions" :get (conj interceptors/common-interceptors `home/instructions-page)]
     ["/about" :get (conj interceptors/common-interceptors `about-page)]
     ["/authorized" :get (conj interceptors/common-interceptors `authorized)]}
  auth.routes/routes))


;; Map-based routes
;(def routes `{"/" {:interceptors [(body-params/body-params) http/html-body]
;                   :get home-page
;                   "/about" {:get about-page}}})

;; Terse/Vector-based routes
;(def routes
;  `[[["/" {:get home-page}
;      ^:interceptors [(body-params/body-params) http/html-body]
;      ["/about" {:get about-page}]]]])


;; Consumed by hackathon.server/create-server
;; See http/default-interceptors for additional options you can configure
(def service {:env :prod
              ;; You can bring your own non-default interceptors. Make
              ;; sure you include routing and set it up right for
              ;; dev-mode. If you do, many other keys for configuring
              ;; default interceptors will be ignored.
              ;; ::http/interceptors []
              ::http/routes routes

              ;; Uncomment next line to enable CORS support, add
              ;; string(s) specifying scheme, host and port for
              ;; allowed source(s):
              ;;
              ;; "http://localhost:8080"
              ;;
              ;;::http/allowed-origins ["scheme://host:port"]

              ;; Tune the Secure Headers
              ;; and specifically the Content Security Policy appropriate to your service/application
              ;; For more information, see: https://content-security-policy.com/
              ;;   See also: https://github.com/pedestal/pedestal/issues/499
              ::http/secure-headers {:content-security-policy-settings {:default-src "'self' fonts.gstatic.com"
                                                                        :object-src "'self'"
                                                                        :script-src "'self' 'unsafe-inline'"
                                                                        :style-src "'self' 'unsafe-inline' fonts.googleapis.com"
                                                                        :fonts-src "'self' fonts.gstatic.com"}}
;;'unsafe-eval' 'strict-dynamic' https: http:"
              ;;                                                          :frame-ancestors "'none'"}}

              ;; Root for resource interceptor that is available by default.
              ::http/resource-path "/public"

              ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
              ;;  This can also be your own chain provider/server-fn -- http://pedestal.io/reference/architecture-overview#_chain_provider
              ::http/enable-session {:store (cookie/cookie-store {:key secret})} 
              ;; Enables CSRF protection for all POST, PUT, PATCH, DELETE requests
              ;; disable this if you want to fine tune your CSRF handling,
              ;; refer to https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html
              ;; This doesn't work for form-params
              ;;::http/enable-csrf (csrf/anti-forgery)
              ::http/type :jetty
              ;;::http/host "localhost"
              ::http/port 8081
              ;; Options to pass to the container (Jetty)
              ::http/container-options {:h2c? true
                                        :h2? false
                                        ;:keystore "test/hp/keystore.jks"
                                        ;:key-password "password"
                                        ;:ssl-port 8443
                                        :ssl? false}})

