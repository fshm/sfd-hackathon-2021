(ns hackathon.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [hackathon.site :as site]
            [hackathon.db :as db]
            [hackathon.site.db :as site.db]
            [hackathon.site.forms :as site.forms]))

(defn home-page [request]
  (ring-resp/response
   (site/base
    "SFD Hackathon 2021"
    [:section.page-section.clearfix
     [:div.container
      [:div.intro
       [:img.intro-img.img-fluid.mb-3.mb-lg-0.rounded
        {:src "/img/intro.jpg" :alt "..."}]
       [:div.intro-text.left-0.text-center.bg-faded.p-5.rounded
        [:h2.section-heading.mb-4
         [:span.section-heading-upper.mb-1 "FSHM Proudly Presents"]
         [:span.section-heading-lower "SFD Hackathon"]]
        [:p.mb-3 "To celebrate the software freedom day, our community has planned to conduct a 36hrs hackathon. SFD Hackathon 2021 will be coming to you on "
         [:strong "September 18-19,2021"]
         ". We are excited to join you on software freedom day. Invite your friends and have fun together."]
        [:div.intro-button.mx-auto [:a.btn.btn-primary.btn-xl {:href "/registration"} "Register Now"]]]]]]
    [:section.page-section.cta
     [:div.container
      [:div.row
       [:div.col-xl-9.mx-auto
        [:div.cta-inner.bg-faded.text-center.rounded
         [:h2.section-heading.mb-4 [:span.section-heading-lower "Prizes"] [:span.section-heading-upper "Prize pool worth: 5000 rupees"]] [:p.mb-0]
         [:ul [:li [:strong "1st Prize worth: 3000 rupees."]]
          [:li [:strong "2nd Prize worth: 2000 rupees."]]
          [:li [:strong "Cool Swags will be given to all participants."]]
          [:li [:strong "Participants Certificate will be provided to successfully submited teams."]]]]]]]])))

(defn instructions-page [request]
  (ring-resp/response
   (site/base
    "SFD Hackathon 2021"
    [:section.page-section.cta
     [:div.container
      [:div.row
       [:div.col-xl-9.mx-auto
        [:div.cta-inner.bg-faded.text-center.rounded [:h2.section-heading.mb-5 [:span.section-heading-upper "SFD Hackathon 2021"] [:span.section-heading-lower "Shedule"]]
         [:ul.list-unstyled.list-hours.mb-5.text-left.mx-auto [:li.list-unstyled-item.list-hours-item.d-flex [:strong "Date and Time"] [:span.ms-auto [:strong "Event"]]]
          [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 18, 09:00 AM
                                    " [:span.ms-auto "Reporting Time"]] [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 18, 10:00 AM
                                    " [:span.ms-auto "Opening Ceremony"]] [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 18, 12:00 PM
                                    " [:span.ms-auto "Hackathon Begins"]] [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 19, 10:00 PM
                                    " [:span.ms-auto "Soft Deadline for submission"]] [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 19, 11:59 PM
                                    " [:span.ms-auto "Hackathon Ends"]] [:li.list-unstyled-item.list-hours-item.d-flex "
                                    Sept 26, 12:00 AM
                                    " [:span.ms-auto "Results will be announced"]]] [:p.address.mb-5 [:em [:strong "HACK TO CHANGE THE WORLD"] [:br] "
                                    Both online and offline participants should follow the same shedule.
                                "]] [:p [:strong "Offline location: "] " Livelihood Resource Center for Marginalized (LRCM), Near Sankara Vidyalaya Hr. Sr. School, ECR Road, Puducherry – 605008."]
         [:p.mb-0 [:small [:em "Open to all, Anyone can partcipate in this hackathon."]]]]]]]]
    [:section.page-section.about-heading
     [:div.container [:img.img-fluid.rounded.about-heading-img.mb-3.mb-lg-0 {:src "/img/about.jpg" :alt "..."}]
      [:div.about-heading-content
       [:div.row
        [:div.col-xl-9.col-lg-10.mx-auto
         [:div.bg-faded.rounded.p-5 [:h2.section-heading.mb-4 [:span.section-heading-upper "INSTRUCTIONS:"]] [:p]
          [:ul [:li.list-unstyled-item "Team members limit: 2 to 4 members."] [:li.list-unstyled-item "Same registration form for both online and offline participants."]
           [:li.list-unstyled-item "Ownership of the project is owned by the team members."]
           [:li.list-unstyled-item "All code will be released exclusively under AGPLv3 (for network / web components) and GPL v3 (for mobile / desktop apps)."]
           [:li.list-unstyled-item "Judging Criterias are Creativity of the idea, UI/UX, Code Readability, Working of the Prototype and usage of FOSS tools."]
           [:li.list-unstyled-item "Decisions taken by the judges are final."]
           [:li.list-unstyled-item "Improper or abusive behaviour from any team, they will be disqualified from the hackathon."]
           [:li.list-unstyled-item "Members participating in this hackathon should bring their own laptops and other hardwares."]
           [:li.list-unstyled-item "Lunch will not be provided to the participants."]]]
         [:div.bg-faded.rounded.p-5.mt-5.text-center
          [:h2 [:span.section-heading-lower [:h1 [:b "THEMES"]]]]]]]]]]
    [:section.page-section
     [:div.container
      [:div.product-item [:div.product-item-title.d-flex [:div.bg-faded.p-5.d-flex.me-auto.rounded [:h2.section-heading.mb-0 [:span.section-heading-lower [:h1 "Entertainment"]]]]]
       [:img.product-item-img.mx-auto.d-flex.rounded.img-fluid.mb-3.mb-lg-0 {:src "/img/entertainment.jpg" :alt "..."}]
       [:div.product-item-description.d-flex.ms-auto
        [:div.bg-faded.p-5.rounded [:p.mb-0] [:h3 [:b "Questions:"]] [:br] [:h4 [:b "1. FOSS Games"]]
         [:p "Develop a free and open source mobile or desktop or web games in any genre."] [:br] [:h4 [:b "2. FOSS Meme Creation App"]]
         [:p "Develop a mobile or web app to create memes and share them."]]]]]]
    [:section.page-section
     [:div.container
      [:div.product-item
       [:div.product-item-title.d-flex
        [:div.bg-faded.p-5.d-flex.me-auto.rounded
         [:h2.section-heading.mb-0
          [:span.section-heading-lower [:h1 "Free Software Movement"]]]]]
       [:img.product-item-img.mx-auto.d-flex.rounded.img-fluid.mb-3.mb-lg-0 {:src "/img/fsm.png" :alt "..."}] [:div.product-item-description.d-flex.ms-auto [:div.bg-faded.p-5.rounded [:p.mb-0] [:h3 [:b "Questions:"]] [:br] [:h4 [:b "1. Free software adoption heat map"]] [:p "
                              Develop a Free software to maintain the data of free software usage in our society.
                          "] [:br] [:h4 [:b "2. Culture recommender (books, movies, games, songs)"]] [:p "
                              Develop a mobile app or web app, which recommend based on your special interests.
                          "] [:br] [:h4 [:b "3. Foss review tool"]] [:p "
                            Develop a free software to review other available free softwares in the tech world.
                        "] [:br] [:h4 [:b "4. Leaderboard (OSM/Wikimedia commons)"]] [:p "
                            Develop a leaderboard website for FSHM to conduct other events like Open street map and wikimedia commons contributions.
                        "]]]]]] [:section.page-section [:div.container [:div.product-item [:div.product-item-title.d-flex [:div.bg-faded.p-5.d-flex.me-auto.rounded [:h2.section-heading.mb-0 [:span.section-heading-lower [:h1 "Everyday Life"]]]]] [:img.product-item-img.mx-auto.d-flex.rounded.img-fluid.mb-3.mb-lg-0 {:src "/img/everyday.webp" :alt "..."}] [:div.product-item-description.d-flex.ms-auto [:div.bg-faded.p-5.rounded [:p.mb-0] [:h3 [:b "Questions:"]] [:br] [:h4 [:b "1. App usage tracker"]] [:p "
                              Develop a mobile app to monitor and restrict the usage of social medias and video games on the user's smartphone.
                          "] [:br] [:h4 [:b "2. Measuring Objects via CV / AR / VR"]] [:p "
                              Develop a mobile app which calculates the dimensions of the object by using computer vision.
                          "] [:br] [:h4 [:b "3. Scan documents into OCR & align / crop"]] [:p "
                              Develop a mobile app to scan the written or printed documents with align and crop feature.
                          "] [:br] [:h4 [:b "4. Dating App"]] [:p "
                              Develop a mobile dating app with recommendation feature based on your interest.
                          "] [:br] [:h4 [:b "5. FOSS UPI payment app"]] [:p "
                              Develop a free and opensource UPI payment app (Alternative for Gpay,Phonepe and Paytm).
                          "] [:br] [:h4 [:b "6. wiktionary linked offline dictionary app"]] [:p "
                              Develop a offline dictionary mobile app using wiktionary.
                          "] [:br] [:h4 [:b "7. Rideshare / Ride aggregation app"]] [:p "
                              Develop a taxi booking mobile app (Alternative for Uber and Ola).
                          "] [:br] [:h4 [:b "8. Amazon FOSS Client"]] [:p "
                              Develop a FOSS client to shop and purchase on Amazon website.
                          "] [:br] [:h4 [:b "9. Specs Selector using AR"]] [:p "
                              Develop a FOSS mobile app to select suitable specs for your face using Augmented reality.
                          "]]]]]] [:section.page-section [:div.container [:div.product-item [:div.product-item-title.d-flex [:div.bg-faded.p-5.d-flex.me-auto.rounded [:h2.section-heading.mb-0 [:span.section-heading-lower [:h1 "Hardware"]]]]] [:img.product-item-img.mx-auto.d-flex.rounded.img-fluid.mb-3.mb-lg-0 {:src "/img/hardware.jpg" :alt "..."}] [:div.product-item-description.d-flex.ms-auto [:div.bg-faded.p-5.rounded [:p.mb-0] [:h3 [:b "Questions:"]] [:br] [:h4 [:b "1. Free CAD Model"]] [:p "
                              Design 3d models in Free CAD software.
                          "] [:br] [:h4 [:b "2. Weather station"]] [:p "
                              Create a weather station to calculate the atmosphere condition.
                          "] [:br] [:h4 [:b "3. DC UPS"]] [:p "
                              Create a DC UPS to store power.
                          "] [:br] [:h4 [:b "4. Drone"]] [:p "
                              Solve any real life problem with drones.
                          "]]]]]])))

(defn registration-page [request]
  (ring-resp/response
   (site/base
    "SFD Hackathon 2021"
    [:div
     [:section.page-section.cta
      [:div.container
       [:div.row
        [:div.col-xl-9.mx-auto
         [:div.cta-inner.mx-auto.bg-faded.rounded.wrapper.w-100
          [:form {:action "/registration" :method "post"}
           (site/form-anti-forgery-token request)
           [:h2.section-heading.mb-5.text-center [:span.section-heading-lower "Registration"]]
           [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-users]]
            [:input.form-control {:required "yes" :type "text" :placeholder "Team Name" :name "team_name"}]]
           [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]] [:input.form-control {:required "yes" :type "email" :placeholder "Team Email" :name "team_email"}]]
           [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-phone]] [:input.form-control {:required "yes" :type "tel" :placeholder "Team Phone" :name "team_phone" :max_length 10}]]
           [:div.icon [:span.fas.fa-globe-americas {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Participation Mode"]]
           [:select.form-select.mt-3.mb-3 {:required "required" :name "team_mode" :aria-label "Default select example"} [:option {:value ""} "--Select--"] [:option {:value "online"} "Online"] [:option {:value "offline"} "Offline"]]
           [:div [:div.icon [:span.fas.fa-street-view {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Team Size"]]
            [:div.row
             [:div#duo
              [:div.col-md-6 [:input#five {:name "team_size" :type "radio" :value 2}] [:label.box.fifth.w-100 {:for "five"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Duo (2 members)"]]]]]
             [:div#trio [:div.col-md-6 [:input#six {:name "team_size" :type "radio" :value 3}] [:label.box.sixth.w-100 {:for "six"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} " Trio (3 members) "]]]]]
             [:div#squad [:div.col-md-6 [:input#seven {:name "team_size" :type "radio" :value 4}] [:label.box.seveth.w-100 {:for "seven"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} " Squad (4 members) "]]]]]]]
           [:div#members.mt-5.mb-3
            (site.forms/duo-form)
            (site.forms/trio-form)
            (site.forms/quad-form)]
           [:input.btn.btn-primary.mb-3 {:type "submit" :value "Submit"}]]]]]]]])))

(defn submission-page [request]
  (ring-resp/response (site/base "SFD Hackathon 2021")))

(defn registration-post [request]
  (let [form-params (:form-params request)]
    (site.db/registrations-insert db/jdbc
                                  (assoc form-params :team_size (Integer/parseInt (:team_size form-params))))
    (ring-resp/redirect "/")))

(defn submission-post [request]
  (ring-resp/response (site/base "SFD Hackathon 2021")))
