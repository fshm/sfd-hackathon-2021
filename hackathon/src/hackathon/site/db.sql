-- :name registrations-insert :! :n
-- :doc Insert a single registration with only ref_no, title
insert into registrations
values (:team_name, :team_email, :team_phone, :team_size, :team_mode,
:m1_name, :m1_email, :m1_gender, :m1_occupation, :m1_institute,
:m2_name, :m2_email, :m2_gender, :m2_occupation, :m2_institute,
:m3_name, :m3_email, :m3_gender, :m3_occupation, :m3_institute,
:m4_name, :m4_email, :m4_gender, :m4_occupation, :m4_institute)

-- :name registrations-all :? :*
-- :doc Get all registrations
select * from registrations

-- :name registrations-by-team-email :? :1
-- :doc Get Registration by Team Email
select * from registrations
where team_email = :team_email

-- :name registrations-by-team-phone :? :1
-- :doc Get Registration by Team phone
select * from registrations
where team_phone = :team_phone

-- :name registrations-offline :? :*
-- :doc Get registrations for offline mode
select * from registrations
where team_mode = 'Offline'

-- :name registrations-online :? :*
-- :doc Get registrations for online mode
select * from registrations
where team_mode = 'Online'

-- :name registrations-duo :? :*
-- :doc Get registrations for online mode
select * from registrations
where team_mode = 'Online'
