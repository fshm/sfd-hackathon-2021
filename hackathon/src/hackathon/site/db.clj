(ns hackathon.site.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hackathon/site/db.sql")
