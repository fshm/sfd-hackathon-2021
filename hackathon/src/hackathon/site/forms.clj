(ns hackathon.site.forms)

(defn duo-form []
  [:div#sub-duo
   [:div#member-1
    [:div.text-center
     [:span {:style "font-size: 1.5rem;"}]
     [:span {:style "margin-left: 12px; font-size: 1.5rem;"} [:strong "Leader"]]]
    [:div#member-3 [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-user]]
                    [:input.form-control {:name "m1_name" :required "required"  :type "text" :placeholder "Leader Name"}]]
     [:div.icon [:span.fas.fa-tshirt {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Gender"]]
     [:div.row.mb-3 [:div.col-md-6 [:input#one {:type "radio" :name "m1_gender" :value "Male" :required "required"}]
                     [:label.box.first.w-100 {:for "one"}
                      [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Male"]]]]
      [:div.col-md-6 [:input#two {:type "radio" :name "m1_gender" :value "Female"}]
       [:label.box.second.w-100 {:for "two"}
        [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Female "]]]]
      [:div.col-md-6 [:input#three {:type "radio" :name "m1_gender" :value "Transgender"}]
       [:label.box.third.w-100 {:for "three"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Transgender"]]]]
      [:div.col-md-6 [:input#four {:type "radio" :name "m1_gender" :value "Queer"}]
       [:label.box.forth.w-100 {:for "four"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Queer"]]]]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]] [:input.form-control {:name "m1_email" :required "required" :type "email" :placeholder "E-Mail Address"}]] [:div.icon [:span.fas.fa-university {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Occupation"]]
     [:select.form-select.mt-3.mb-3 {:name "m1_occupation" :required "required" :aria-label "Default select example"} [:option {:value ""} "--Select--"] [:option {:value "student"} "Student"] [:option {:value "non-working"} "Non-Working"] [:option {:value "working"} "Working"]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-graduation-cap]]
      [:input.form-control {:name "m1_institute" :required "required" :type "text" :placeholder "Institution/College"}]]]]
   [:div#member-2 [:div.text-center [:span {:style "font-size: 1.5rem;"}] [:span {:style "margin-left: 12px; font-size: 1.5rem;"} [:strong.mt-2 "Member 2"]]]
    [:div#member-3 [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-user]]
                    [:input.form-control {:name "m2_name" :required "required" :type "text" :placeholder "Member 2 Name"}]]
     [:div.icon [:span.fas.fa-tshirt {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Gender"]]
     [:div.row.mb-3 [:div.col-md-6 [:input#m21 {:name "m2_gender" :required "required" :type "radio" :value "Male"}]
                     [:label.box.m211.w-100 {:for "m21"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Male"]]]]
      [:div.col-md-6 [:input#m22 {:type "radio" :name "m2_gender" :value "Female"}] [:label.box.m222.w-100 {:for "m22"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Female "]]]] [:div.col-md-6 [:input#m23 {:type "radio" :name "m2_gender" :value "Transgender"}] [:label.box.m233.w-100 {:for "m23"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Transgender"]]]] [:div.col-md-6 [:input#m24 {:type "radio" :name "m2_gender" :value "Queer"}] [:label.box.m244.w-100 {:for "m24"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Queer"]]]]]
     [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]]
      [:input.form-control {:name "m2_email" :required "required" :type "email" :placeholder "E-Mail Address"}]]
     [:div.icon [:span.fas.fa-university {:style "font-size: 1.1rem;"}]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Occupation"]]
     [:select.form-select.mt-3.mb-3 {:name "m2_occupation" :required "required" :aria-label "Default select example"}
      [:option {:value ""} "--Select--"] [:option {:value "student"} "Student"]
      [:option {:value "non-working"} "Non-Working"]
      [:option {:value "working"} "Working"]]
     [:div.form-group.d-flex.align-items-center
      [:div.icon [:span.fas.fa-graduation-cap]] [:input.form-control {:name "m2_institute" :required "required" :type "text" :placeholder "Institution/College"}]]]]])

(defn trio-form []
  [:div#sub-tri.noFill
   [:div.text-center [:span {:style "font-size: 1.5rem;"}]
    [:span {:style "margin-left: 12px; font-size: 1.5rem;"} [:strong.mt-2 "Member 3"]]]
   [:div#member-3 [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-user]]
                   [:input.form-control {:name "m3_name" :type "text" :placeholder "Member 3 Name"}]]
    [:div.icon [:span.fas.fa-tshirt {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Gender"]]
    [:div.row.mb-3
     [:div.col-md-6 [:input#m31 {:type "radio" :name "m3_gender" :value "Male"}]
      [:label.box.m311.w-100 {:for "m31"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Male"]]]]
     [:div.col-md-6 [:input#m32 {:type "radio" :name "m3_gender" :value "Female" :checked "checked"}
                     [:label.box.m322.w-100 {:for "m32"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Female "]]]]]
     [:div.col-md-6 [:input#m33 {:type "radio" :name "m3_gender" :value "Transgender"}]
      [:label.box.m333.w-100 {:for "m33"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Transgender "]]]]
     [:div.col-md-6 [:input#m34 {:type "radio" :name "m3_gender" :value "Queer"}] [:label.box.m344.w-100 {:for "m34"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Queer"]]]]]
    [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]]
     [:input.form-control {:name "m3_email" :type "email" :placeholder "E-Mail Address"}]]
    [:div.icon [:span.fas.fa-university {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Occupation"]] [:select.form-select.mt-3.mb-3 {:name "m3_occupation" :aria-label "Default select example"} [:option {:value ""} "--Select--"] [:option {:value "student"} "Student"] [:option {:value "non-working"} "Non-Working"] [:option {:value "working"} "Working"]]
    [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-graduation-cap]]
     [:input.form-control {:name "m3_institute" :type "text" :placeholder "Institution/College"}]]]])

(defn quad-form []
  [:div#sub-squad.noFill
   [:div#member-4
    [:div.text-center
     [:span {:style "font-size: 1.5rem;"}]
     [:span {:style "margin-left: 12px; font-size: 1.5rem;"} [:strong "Member 4"]]]
    [:div#member-3 [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-user]]
                    [:input.form-control {:name "m4_name" :type "text" :placeholder "Member 4 Name"}]]
     [:div.icon [:span.fas.fa-tshirt {:style "font-size: 1.1rem;"}] [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Gender"]]
     [:div.row.mb-3
      [:div.col-md-6 [:input#m41 {:name "m4_gender" :value "Male" :type "radio"}]
       [:label.box.m411.w-100 {:for "m41"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Male"]]]]
      [:div.col-md-6 [:input#m42 {:name "m4_gender" :value "Female" :type "radio" :checked "checked"}
                      [:label.box.m422.w-100 {:for "m42"}
                       [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Female "]]]]]
      [:div.col-md-6
       [:input#m43 {:name "m4_gender" :value "Transgender" :type "radio"}] [:label.box.m433.w-100 {:for "m43"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Transgender"]]]]
      [:div.col-md-6 [:input#m44 {:name "m4_gender" :value "Queer" :type "radio"}] [:label.box.m444.w-100 {:for "m44"} [:div.course [:span.circle] [:span.subject {:style "font-size: 1.1rem;"} "Queer"]]]]]
      [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-envelope]] [:input.form-control {:name "m4_email" :type "email" :placeholder "E-Mail Address"}]]
      [:div.icon [:span.fas.fa-university {:style "font-size: 1.1rem;"}]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                [:span {:style "margin-left: 12px; font-size: 1.1rem;"} "Occupation"]]
      [:select.form-select.mt-3.mb-3 {:name "m4_occupation" :aria-label "Default select example"} [:option {:value ""} "--Select--"] [:option {:value "student"} "Student"]
       [:option {:value "non-working"} "Non-Working"] [:option {:value "working"} "Working"]]
      [:div.form-group.d-flex.align-items-center [:div.icon [:span.fas.fa-graduation-cap]]
       [:input.form-control {:name "m4_institute" :type "text" :placeholder "Institution/College"}]]]]])
