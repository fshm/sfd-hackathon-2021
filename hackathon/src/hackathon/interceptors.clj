(ns hackathon.interceptors
    (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [io.pedestal.http.csrf :as csrf]
            [io.pedestal.interceptor :as interceptor]
            [io.pedestal.interceptor.chain :as interceptor.chain]
            [io.pedestal.interceptor.error :refer [error-dispatch]]
            [ring.util.response :as ring-resp]
            [ring.middleware.session.cookie :as cookie]
            [buddy.auth :as auth]
            [buddy.auth.backends :as auth.backends]
            [buddy.auth.middleware :as auth.middleware]
            [cheshire.core :as json]
            ;; [mount.core :refer defstate]
            ))


(def secret (:secret (clojure.edn/read-string (slurp "resources/env/secret.edn"))))

(defn unauthorized-handler
  "This is the default action for buddy-auth's unathorized exception."
  [request metadata]
  (ring-resp/redirect "/"))

(def backend (auth.backends/session {:unauthorized-handler unauthorized-handler}))


(def authorized-interceptor
  "Port of buddy-auth's wrap-authentication middleware."
  (interceptor/interceptor
   {:name ::authorized-interceptor
    :leave (fn [ctx]
             (let [request (:request ctx)]
               (if-not (boolean (:identity request))
                 (assoc ctx :response {:status 403
                                       :body "Please login"})
                 ctx)))}))


(def content-length-json-body
  (interceptor/interceptor
   {:name ::content-length-json-body
    :leave (fn [context]
             (let [response (:response context)
                   body (:body response)
                   json-response-body (if body (json/generate-string body) "")
                    ;; Content-Length is the size of the response in bytes
                    ;; Let's count the bytes instead of the string, in case there are unicode characters
                   content-length (count (.getBytes ^String json-response-body))
                   headers (:headers response {})]
               (assoc context
                      :response {:status (:status response)
                                 :body json-response-body
                                 :headers (merge headers
                                                 {"Content-Type" "application/json;charset=UTF-8"
                                                  "Content-Length" (str content-length)})})))}))


;; https://github.com/pedestal/pedestal/blob/master/samples/buddy-auth/src/buddy_auth/service.clj
(def authentication-interceptor
  "Port of buddy-auth's wrap-authentication middleware."
  (interceptor/interceptor
   {:name ::authentication-interceptor
    :enter (fn [ctx]
             (update
              ctx
              :request auth.middleware/authentication-request backend))}))

(defn authorization-interceptor
  "Port of buddy-auth's wrap-authorization middleware."
  [backend]
  (error-dispatch [ctx ex]
                  [{:exception-type :clojure.lang.ExceptionInfo :stage :enter}]
                  (try
                    (assoc ctx
                           :response
                           (auth.middleware/authorization-error (:request ctx)
                                                                 ex
                                                                backend))
                    (catch Exception e
                      (assoc ctx ::interceptor.chain/error e)))

                  :else (assoc ctx ::interceptor.chain/error ex)))

;; Defines "/" and "/about" routes with their associated :get handlers.
;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children (/about).
(def common-interceptors [(body-params/body-params) http/html-body (csrf/anti-forgery)
                          authentication-interceptor (authorization-interceptor backend)])
(def json-interceptors [(body-params/body-params) content-length-json-body])
(def file-interceptors [(body-params/body-params) (middlewares/multipart-params)])
;; Use this to specify CSRF protection for specific requests
(def anti-forgery-interceptors [(body-params/body-params) http/html-body (csrf/anti-forgery)])
