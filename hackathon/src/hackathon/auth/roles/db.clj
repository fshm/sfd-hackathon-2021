(ns hackathon.auth.roles.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hackathon/auth/roles/roles.sql")
