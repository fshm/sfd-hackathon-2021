-- :name insert-role :! :n
-- :doc Insert a role
insert into roles
values :role, :description

-- :name get-role :? :1
-- :doc Get role by role-name
select * from roles
where role = :role

-- :name delete-role :! :n
-- :doc Delete a role
delete from roles
where role = :role
