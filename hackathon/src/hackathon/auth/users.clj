(ns hackathon.auth.users
    (:require [buddy.hashers :as hashers]
              [hackathon.db :as db]
              [hackathon.auth :as auth]
              [hackathon.auth.users.db :as users]))

(defn user-by-email-exists?
  "Checks if user for the given email exists"
  [email]
  (let [data (users/user-by-email db/jdbc email)]
    (nil? data)))

(defn user-by-username-exists?
  "Checks if user for the given username exists"
  [username]
  (let [data (users/user-by-email db/jdbc username)]
    (nil? data)))

(defn user-exists?
  "Checks if user for the given username or email exists"
  [user]
  (let [data (users/user-by-email-or-username db/jdbc user)]
    (nil? data)))

(defn valid-email?
  "Checks if given data is valid string and email"
  [email]
  (let [pattern #"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"]
    (if (= email (and (string? email) (re-matches pattern email)))
      true
      false)))

(defn valid-new-user?
  "Runs input validations for a map containing username, email & password."
  [data]
  (if
   (= true
      (string? (:username data))
      (string? (:email data))
      (string? (:password data))
      (valid-email? (:email data)))
  true
  false))

(defn new-user
  "Creates a new user in the db using username, email & password. Password should be non-hashed."
  [data & role]
  (let [datum
        {:username (:username data)
         :email (:email data)
         :password (auth/hash-password (:password data))
         :role (if (string? role) role "user")}]
    (users/insert-user  db/jdbc datum)))
