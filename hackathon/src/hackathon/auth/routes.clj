(ns hackathon.auth.routes
  (:require
   [hackathon.auth.users.site :as users.site]
   [hackathon.interceptors :as interceptors]))

(def routes
  #{["/register" :get (conj interceptors/common-interceptors `users.site/register-page) :route-name :register-page]
    ["/register" :post (conj interceptors/anti-forgery-interceptors `users.site/register-post) :route-name :register-post]
    ["/login" :get (conj interceptors/common-interceptors `users.site/login-page) :route-name :login-page]
    ["/login" :post (conj interceptors/common-interceptors `users.site/login-post) :route-name :login-post]
    ["/logout" :get (conj interceptors/common-interceptors `users.site/logout) :route-name :logout]})
