(ns hackathon.auth.users.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "hackathon/auth/users/users.sql")
