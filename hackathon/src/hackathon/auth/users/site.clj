(ns hackathon.auth.users.site
    (:require [ring.util.response :as ring-resp]
              [hackathon.site :as site]
              [hackathon.auth :as auth]
              [hackathon.auth.users.forms :as forms]
              [hackathon.auth.users :as auth.users]
              [buddy.auth :as ba]))

(defn login-page [request]
  (ring-resp/response
   (site/base
    "Login"
    (forms/login-control request))))

(defn login-post [request]
  (let [data (:form-params request)
        user (auth/authenticate-username-password data)
        session (:session request)]
    (if (nil? user)
      (ring-resp/response "Username or password is incorrect")
      (let [updated-session (assoc session
                                   :identity (keyword (:username user))
                                   :role (keyword (:role user)))]
        (-> (ring-resp/redirect "/")
            (assoc :session updated-session))))))

(defn logout
  "Logs out the authenticated session"
  [request]
  (-> (ring-resp/redirect "/")
      (assoc :session {})))

(defn register-page [request]
  (ring-resp/response
   (site/base
    "Register"
    (forms/register-control request))))

(defn register-post [request]
  (let [data (:form-params request)
        session (:session request)]
    (if (= (:password data) (:confirm_password data))
      (if (auth.users/user-exists? data)
        (do (auth.users/new-user data)
            (let [updated-session (assoc session
                                         :identity (keyword (:username data))
                                         :role :user)]
              (-> (ring-resp/redirect "/")
                  (assoc :session updated-session))))
        (ring-resp/response "Username or email already taken"))
      (ring-resp/response "Password doesn't match"))))
