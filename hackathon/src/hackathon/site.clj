(ns hackathon.site
  (:require
   [ring.util.response :as ring-resp]
   [io.pedestal.http.csrf :as csrf])
  (:use
   [hiccup.page :only (html5 include-css include-js)]))

(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1, shrink-to-fit=no"}]
          [:link {:rel "icon" :type "image/png" :href "/img/favicon.png"}]
          [:link {:rel "icon" :type "image/x-icon" :href "assets/icon.png"}]
          [:title title]
          (include-css "/css/styles.css")]
         [:header [:h1.site-heading.text-center.text-faded.d-none.d-lg-block
                   [:span.site-heading-upper.text-primary.mb-3 "Software Freedom Day"]
                   [:span.site-heading-lower "SFD Hackathon 2021"]]]
         [:nav#mainNav.navbar.navbar-expand-lg.navbar-dark.py-lg-4
          [:div.container [:a.navbar-brand.text-uppercase.fw-bold.d-lg-none {:href "index.html"} "SFD Hackathon"]
           [:button.navbar-toggler {:type "button" :data-bs-toggle "collapse" :data-bs-target "#navbarSupportedContent" :aria-controls "navbarSupportedContent" :aria-expanded "false" :aria-label "Toggle navigation"} [:span.navbar-toggler-icon]]
           [:div#navbarSupportedContent.collapse.navbar-collapse
            [:ul.navbar-nav.mx-auto
             [:li.nav-item.px-lg-4 [:a.nav-link.text-uppercase {:href "/"} "Home"]]
             [:li.nav-item.px-lg-4 [:a.nav-link.text-uppercase {:href "/instructions"} "Instructions"]]
             [:li.nav-item.px-lg-4 [:a.nav-link.text-uppercase {:href "/registration"} "Registration"]]
             [:li.nav-item.px-lg-4 [:a.nav-link.text-uppercase {:href "/membership"} "Membership"]]
             ]]]]
         content
         [:footer.footer.text-faded.text-center.py-5 [:div.container [:p.m-0.small "Copyright © FSHM 2021"]]]
         (include-css "/css/Lora.css")
         (include-css "/css/Raleway.css")
         (include-js "/js/bootstrap-min.js")
         (include-js "/js/fontawesome.js")
         ;;(include-js "/js/scripts.js")
         ))

(defn form-control-input
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "text" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-textarea
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:textarea.textarea {:type "text" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-email
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "email" :placeholder text :name param-name :value value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-date
  "Default form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control
                                   [:input.input {:type "date" :name param-name :value value}]]
   [:p.help "Click to choose a date"]])

(defn form-control-password
  "Password form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.label text] [:div.control [:input.input {:type "password" :placeholder text :name param-name :value  value}]]
   ;;[:p.help "This is a help text"]
   ])

(defn form-control-checkbox
  "Checkbox form control for dynamic deployment"
  [text param-name & value]
  [:div.field [:label.checkbox text] [:div.control [:input {:type "checkbox" :placeholder text :name param-name :value value}]]])

(defn form-hidden-value
  "Default form control for dynamic deployment"
  [param-name & value]
  [:input {:type "hidden" :name param-name :value value}])

(defn form-anti-forgery-token
  "CSRF anti-forgery-token"
  [request]
  (form-hidden-value "__anti-forgery-token" (csrf/anti-forgery-token request)))

(defn form-control-dropdown
  "Dropdown form control for dynamic deployment. Accepts label name, form value name and value pairs for each option."
  [text name option-pairs]

  [:div.field [:label.label text]
   [:div.control
    [:div.select.is-primary
     [:select {:name name}
      (for [center option-pairs]
        [:option {:value (second center)} (first center)])]]]])

;;      (for [x options y values]
;;       [:option {:value (str y)} (str x)])]]]])


(defn form-control-button
  "Button control for a form"
  [name]
  [:div.control [:button.button.is-primary.is-medium name]])

(defn notification-success
  "Notification control"
  [message]
  [:div.notification.is-primary message
   [:button.delete]])

(defn notification-failure
  "Notification control"
  [message]
  [:div.notification.is-danger message
   [:button.delete]])
