window.addEventListener('DOMContentLoaded', event => {
  const listHoursArray = document.body.querySelectorAll('.list-hours li');
  let j = 1;
  if (new Date().getDate() >= '18') {
    if (new Date().getHours() >= '9') {
      j= 2;
    }
    if (new Date().getHours() >= '10') {
      j= 3;
    }
    if (new Date().getHours() >= '12') {
      j= 4;
    }
  }
if (new Date().getDate() >= '19') {
  j= 4;
  if (new Date().getHours() >= '22') {
    j= 5;
  }
}
if (new Date().getDate() >= '20') {
  j= 6;
}
  listHoursArray[j].classList.add(('today'));
})

const duoContent = `
        <div id="sub-duo">
            <div id="member-1">
            <div class="text-center">
            <span style="font-size: 1.5rem;"></span><span style="margin-left: 12px; font-size: 1.5rem;"><strong>Leader</strong></span></div>
                <div id="member-3">
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-user"></span></div> <input name="m1_name" autocomplete="off" type="text" class="form-control" placeholder="Leader Name" required>
                    </div>
                    <div class="icon"><span class="fas fa-tshirt" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Gender</span> </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6"> <input type="radio" name="m1_gender" value="Male" id="one" required> <label for="one" class="box first w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Male</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m1_gender" value="Female" id="two"> <label for="two" class="box second w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Female </span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m1_gender" value="Transgender" id="three"> <label for="three" class="box third w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Transgender</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio"  name="m1_gender" value="Queer" id="four"> <label for="four" class="box forth w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Queer</span>
                                                </label> </div></div>
                                        </div>

<div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-envelope"></span></div> <input name="m1_email" autocomplete="off" type="email" class="form-control" placeholder="E-Mail Address" required>
                    </div>
                    <div class="icon"><span class="fas fa-university" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Occupation</span> </div>
                    <select name="m1_occupation" class="form-select mt-3 mb-3" aria-label="Default select example" required>
                        <option value="">--Select--</option>
                        <option value="student">Student</option>
                        <option value="non-working">Non-Working</option>
                        <option value="working">Working</option>
                        </select>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-graduation-cap"></span></div> <input name="m1_institute" autocomplete="off" type="textr" class="form-control" placeholder="Institution/College" required>
                    </div>
                </div>
            </div>
            <div id="member-2">
            <div class="text-center">
            <span style="font-size: 1.5rem;"></span><span style="margin-left: 12px; font-size: 1.5rem;"><strong class="mt-2">Member 2</strong></span></div>
                <div id="member-3">
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-user"></span></div> <input name="m2_name" autocomplete="off" type="text" class="form-control" placeholder="Member 2 Name" required>
                    </div>
                    <div class="icon"><span class="fas fa-tshirt" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Gender</span> </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6"> <input name="m2_gender" type="radio"  name="m2_gender" value="Male"  id="m21" required> <label for="m21" class="box m211 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Male</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m2_gender" value="Female" id="m22"> <label for="m22" class="box m222 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Female </span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m2_gender" value="Transgender" " id="m23"> <label for="m23" class="box m233 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Transgender</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m2_gender" value="Queer" id="m24"> <label for="m24" class="box m244 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Queer</span>
                                                </label> </div></div>
                                        </div>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-envelope"></span></div> <input name="m2_email" autocomplete="off" type="email" class="form-control" placeholder="E-Mail Address" required>
                    </div>
                    <div class="icon"><span class="fas fa-university" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Occupation</span> </div>
                    <select name="m2_occupation" class="form-select mt-3 mb-3" aria-label="Default select example" required>
                        <option value="">--Select--</option>
                        <option value="student">Student</option>
                        <option value="non-working">Non-Working</option>
                        <option value="working">Working</option>
                        </select>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-graduation-cap"></span></div> <input name="m2_institute" autocomplete="off" type="textr" class="form-control" placeholder="Institution/College" required>
                    </div>

                </div>
            </div>
        </div>
        `;
        const trioContent = duoContent +
            `
            <div id="sub-tri" class="noFill">
            <div class="text-center">
            <span style="font-size: 1.5rem;"></span><span style="margin-left: 12px; font-size: 1.5rem;"><strong class="mt-2">Member 3</strong></span></div>
                <div id="member-3">
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-user"></span></div> <input name="m3_name" autocomplete="off" type="text" class="form-control" placeholder="Member 3 Name" required>
                    </div>
                    <div class="icon"><span class="fas fa-tshirt" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Gender</span> </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6"> <input type="radio" name="m3_gender" value="Male" id="m31" required> <label for="m31" class="box m311 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Male</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m3_gender" value="Female" id="m32"> <label for="m32" class="box m322 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Female </span>
                                                </label> </div></div>
                                                <div class="col-md-6"> <input type="radio" name="m3_gender" value="Transgender" id="m33"> <label for="m33" class="box m333 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Transgender </span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input type="radio" name="m3_gender" value="Queer" id="m34"> <label for="m34" class="box m344 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Queer</span>
                                                </label> </div></div>
                                        </div>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-envelope"></span></div> <input name="m3_email" autocomplete="off" type="email" class="form-control" placeholder="E-Mail Address" required>
                    </div>
                    <div class="icon"><span class="fas fa-university" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Occupation</span> </div>
                    <select name="m3_occupation" class="form-select mt-3 mb-3" aria-label="Default select example" required>
                        <option value="">--Select--</option>
                        <option value="student">Student</option>
                        <option value="non-working">Non-Working</option>
                        <option value="working">Working</option>
                        </select>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-graduation-cap"></span></div> <input name="m3_institute" autocomplete="off" type="textr" class="form-control" placeholder="Institution/College" required>
                    </div>
                </div>
            </div>
        `;
        const squadContent = trioContent + `
        <div id="sub-squad" class="noFill">
            <div id="member-4">
            <div class="text-center">
            <span style="font-size: 1.5rem;"></span><span style="margin-left: 12px; font-size: 1.5rem;"><strong >Member 4</strong></span></div>
                <div id="member-3">
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-user"></span></div> <input name="m4_name" autocomplete="off" type="text" class="form-control" placeholder="Member 4 Name" required>
                    </div>
                    <div class="icon"><span class="fas fa-tshirt" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Gender</span> </div>
                                        <div class="row mb-3">
                                            <div class="col-md-6"> <input name="m4_gender" value="Male" type="radio" name="m-4" id="m41" required> <label for="m41" class="box m411 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Male</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input name="m4_gender" value="Female" type="radio" name="m-4" id="m42"> <label for="m42" class="box m422 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Female </span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input name="m4_gender" value="Transgender" type="radio" name="m-4" id="m43"> <label for="m43" class="box m433 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Transgender</span>
                                                </label> </div></div>
                                            <div class="col-md-6"> <input name="m4_gender" value="Queer" type="radio" name="m-4" id="m44"> <label for="m44" class="box m444 w-100">
                                                    <div class="course"> <span class="circle"></span> <span class="subject" style="font-size: 1.1rem;">Queer</span>
                                                </label> </div></div>
                                        </div>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-envelope"></span></div> <input name="m4_email" autocomplete="off" type="email" class="form-control" placeholder="E-Mail Address" required>
                    </div>
                    <div class="icon"><span class="fas fa-university" style="font-size: 1.1rem;"></span><span style="margin-left: 12px; font-size: 1.1rem;">Occupation</span> </div>
                    <select name="m4_occupation" class="form-select mt-3 mb-3" aria-label="Default select example" required>
                        <option value="">--Select--</option>
                        <option value="student">Student</option>
                        <option value="non-working">Non-Working</option>
                        <option value="working">Working</option>
                        </select>
                    <div class="form-group d-flex align-items-center">
                        <div class="icon"><span class="fas fa-graduation-cap"></span></div> <input name="m4_institute" autocomplete="off" type="textr" class="form-control" placeholder="Institution/College" required>
                    </div>
                </div>
            </div>
                    </div>
        `
        document.getElementById("duo").addEventListener("click", () => {
            document.getElementById("members").innerHTML = duoContent;
        });
        document.getElementById("trio").addEventListener("click", () => {
            document.getElementById("members").innerHTML = trioContent;
        });
        document.getElementById("squad").addEventListener("click", () => {
            document.getElementById("members").innerHTML = squadContent;
        });
